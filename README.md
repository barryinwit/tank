### 平台简介

    心静如水，代码如飞Tank后台管理致力做到简单易懂易学针对初学者锻炼的脚手架，
### 系统说明
     一个简单的基于Spring + Spring MVC + Mybatis 集成SSM框架，代码注释非常完善，非常适合初学者学习，希望能帮到大家！
### 后期计划（由于时间有限后期主要开发Spring boot 和Spring Cloud框架）
1. tank-boot 地址：
2. tank-cloud 地址：
### 演示图（待开放）
 
演示地址：http://114.55.173.190:8080/tank/index

文档地址（待开放）：http://www.fuzongle.com
   
## 内置功能

1.  用户列表：管理系统用户登录系统等操作
2.  登录日志：展示管理登录系统的记录位置等信息
3.  操作日志：展示系统用户操作模块信息日志记录
4.  邮件实现：可以发送邮件等信息操作。
5.  任务中心：可以定时触发Cron任务
6.  图片上传：可以上传图片、下载等
7.  Druid监控：监控数据库sql查询等数据
8.  需求大厅：用户可以发布需求等待其他用户查询并抢单
9.  我发布的：用户可以发布自己的需求等
10. 我的订单：用户可以在需求大厅中抢单操作
11. 系统反馈：反馈系统的问题等
## 注意：

1.如果有任何不懂的地方可以咨询我，随时欢迎互相帮助。



### 在线体验
![输入图片说明](https://images.gitee.com/uploads/images/2020/0621/225133_6a6c41d6_1862401.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0602/230916_ec3bee67_1862401.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0621/221115_387a0b6f_1862401.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0621/221132_cb229531_1862401.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0621/221146_d32d8e90_1862401.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0621/221157_a00afe26_1862401.png "屏幕截图.png")
![输入图片说明](https://images.gitee.com/uploads/images/2020/0621/221209_cf7e42e0_1862401.png "屏幕截图.png")





