/**
 * fuzongle
 * @param url
 * @param data
 * @param successFunction
 * @param successErrorFunction
 * @param failureFunction
 */

function myrequest(url,data,type,successFunction,successErrorFunction,failureFunction) {
    $.ajax({
        type: type,
        url:  url,
        data: data,
        dataType: "JSON",
        async: false,
        beforeSend : function(){/*拼接所有的地址*/ },
        success : function(json){
            if (json.code !== 200) {
                //失败
                if (successErrorFunction == null) { return false; }
                successErrorFunction(json);
            } else {
                //成功
                if (successFunction == null) { return false; }
                successFunction(json);
            }
        },
        error : function(XMLHttpRequest){
            if (failureFunction == null) { return false; }
            failureFunction(XMLHttpRequest);
        }
    });

}