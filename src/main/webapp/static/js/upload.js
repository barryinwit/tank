//单张图片上传
$('.pic_path').on('change',function(){
    var filePath = $(this).val(),         //获取到input的value，里面是文件的路径
        fileFormat = filePath.substring(filePath.lastIndexOf(".")).toLowerCase(),
        src = window.URL.createObjectURL(this.files[0]); //转成可以在本地预览的格式
    //data-id
    var id = $(this).data("id");
    // 检查是否是图片
    if( !fileFormat.match(/.png|.jpg|.jpeg/) ) {
        alert('上传错误,文件格式必须为：png/jpg/jpeg');
        return;
    }
    //图片的显示路劲
    $('#img').attr('src',src);
    var formData = new FormData();
    var files = $(this)[0].files;
    if(files.length>0){
        var filepath = files[0].name;
        var extStart = filepath.lastIndexOf(".");
        var ext = filepath.substring(extStart,filepath.length).toUpperCase();
        if(ext!=".PNG"&&ext!=".GIF"&&ext!=".JPG"&&ext!=".JPEG"){
            alert('上传错误,文件格式必须为：png/jpg/jpeg');
            return;
        }
        $.each(files, function(i, file) {
            formData.append('file', file);
        });

    }else if(files.length==0){
        alert('未选择图片！');
        return false;
    }
    formData.append('type', "1");
    $.ajax({//第一步 请求ajac  需要导入  jquery.js的支持
        type: "POST",//这个属性是  get  还是 post
        url: "/admin/picture/upload",//这是请求地址
        data: formData,//这个是form表单提交 data: formData,
        dataType: "json",//这是请求参数格式
        processData: false,  // 告诉jQuery不要去处理发送的数据
        contentType: false,//文本类型
        async: false,//这个是同步  还是异步
        beforeSend : function(){/*拼接所有的地址*/ },
        success : function(json){//回调成功  还是失败
            if (json.code == 200) {
                $('#img'+id).attr('src',"/"+json.data.data);
                $('#pic_paths'+id).val(json.data.data);
                alert("图片上传成功!")
            }
        }
    });
});