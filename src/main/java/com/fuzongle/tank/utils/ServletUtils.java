package com.fuzongle.tank.utils;

import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/24 01:47
 * @Description:  servlet 相关工具类
 */
public class ServletUtils {

    /**
     * 获取reques 对象
     * @return
     */
    public static ServletRequestAttributes getRequestAttributes() {
        RequestAttributes attributes = RequestContextHolder.getRequestAttributes();
        return (ServletRequestAttributes) attributes;
    }

    public static HttpServletRequest  getRequest(){
        return   ServletUtils.getRequestAttributes().getRequest();
    }

}