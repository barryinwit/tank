package com.fuzongle.tank.utils;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/21 00:56
 * @Description: md5 加密工具类
 */
public class MD5Utils {

    //测试方法
    public static void main(String[] args) {
        String md5Str = getMD5Str("需要加密的字符串");
        System.out.println("加密后的字符串："+md5Str);
        String AA = getMD5Str("123456");
        System.out.println(AA);

    }

    /**
     * md5 加密方法
     * @param str
     * @return
     */
    public static String getMD5Str(String str) {
        byte[] digest = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance("md5");
            digest  = md5.digest(str.getBytes(StandardCharsets.UTF_8));
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        //16是表示转换为16进制数
        String md5Str = new BigInteger(1, digest).toString(16);
        return md5Str;
    }



}