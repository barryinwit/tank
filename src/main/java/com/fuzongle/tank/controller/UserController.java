package com.fuzongle.tank.controller;

import com.fuzongle.tank.annotation.OpenLogAnnotation;
import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.constant.SystemConstant;
import com.fuzongle.tank.entity.User;
import com.fuzongle.tank.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/22 23:41
 * @Description: 用户控制层
 */
@Controller
@RequestMapping("/admin/user")
public class UserController {

    @Autowired
    private UserService userService;

    /**
     * 用户列表请求页面
     */
    @GetMapping
    public String userList() {
        return  "/admin/user/list";
    }

    @GetMapping("/list")
    @ResponseBody
    public JsonResult list(Integer page,Integer size,String account,String name) {
        return userService.selectList(account,name,page,size);
    }

    /**
     * 新增用户保存用户
     */
    @PostMapping("/add")
    @ResponseBody
    @OpenLogAnnotation(title = "添加用户", ServiceType = SystemConstant.SERVICE_TYPE_ADD)
    public JsonResult add(User user) {
        return  userService.insert(user);
    }

    /**
     * 编辑时回显数据
     */
    @GetMapping("/edit")
    @ResponseBody
    public Object edit(@RequestParam Integer id) {
      return JsonResult.success(userService.selectById(id));
    }

    /**
     *编辑用户保存
     */
    @PostMapping("/edit")
    @ResponseBody
    @OpenLogAnnotation(title = "编辑用户", ServiceType = SystemConstant.SERVICE_TYPE_EDIT)
    public JsonResult edit(User user) {
        return  userService.update(user);
    }

    /**
     * 删除用户
     */
    @GetMapping("/delete")
    @ResponseBody
    @OpenLogAnnotation(title = "删除用户", ServiceType = SystemConstant.SERVICE_TYPE_DELETE)
    public JsonResult delete(@RequestParam Integer id) {
        return  userService.delete(id);
    }

}