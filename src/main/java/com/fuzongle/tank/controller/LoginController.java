package com.fuzongle.tank.controller;

import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/18 22:28
 * @Description: 登录控制层
 */
@Controller
public class LoginController {

    @Autowired
    private UserService userService;


    /**
     * 跳转到登录页面
     */
    @GetMapping
    public String login(){
        return  "login";
    }

    /**
     * 点击登录跳转的页面
     */
    @PostMapping("/login")
    @ResponseBody
    public JsonResult login(String account, String password) throws UnsupportedEncodingException, MessagingException, ClassNotFoundException, InterruptedException { //获取用户输入的帐号密码
        return userService.login(account, password);
    }



}