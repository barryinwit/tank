package com.fuzongle.tank.controller;

import com.fuzongle.tank.annotation.OpenLogAnnotation;
import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.constant.SystemConstant;
import com.fuzongle.tank.entity.Picture;
import com.fuzongle.tank.service.PictureService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/12 23:33
 * @Description:
 */
@Controller
@RequestMapping("/admin/picture")
public class PictureController {



    @Autowired
    private PictureService pictureService;

    /**
     *跳转到图片上传下载
     */
    @GetMapping
    public String picture() {
        return  "/admin/picture/list";
    }

    /**
     * 图片列表
     */
    @GetMapping("/list")
    @ResponseBody
    public JsonResult list(Integer page,Integer size,String name) {
        return pictureService.selectList(page,size,name);
    }

    /**
     * 新增图片
     */
    @PostMapping("/add")
    @ResponseBody
    @OpenLogAnnotation(title = "添加图片", ServiceType = SystemConstant.SERVICE_TYPE_ADD)
    public JsonResult add(Picture picture) {
        return  pictureService.insert(picture);
    }

    /**
     * 上传图片
     */
    @PostMapping("/upload")
    @ResponseBody
    public Object upload(@RequestParam(value = "file") MultipartFile multipartFile, HttpServletRequest request) {
        return pictureService.upload(multipartFile,request);
    }


    /**
     * 下载图片
     */
    @GetMapping("/download")
    public void download(@RequestParam Integer id, HttpServletRequest request, HttpServletResponse response) throws Exception {
        pictureService.download(id,request,response);
    }

    /**
     * 删除操作日志
     */
    @GetMapping("/delete")
    @ResponseBody
    @OpenLogAnnotation(title = "删除图片", ServiceType = SystemConstant.SERVICE_TYPE_DELETE)
    public JsonResult delete(@RequestParam Integer id) {
        return  pictureService.delete(id);
    }
}