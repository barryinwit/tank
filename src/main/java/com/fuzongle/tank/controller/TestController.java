package com.fuzongle.tank.controller;

import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.entity.Test;
import com.fuzongle.tank.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/16 22:54
 * @Description: 测试控制层
 */
@Controller
public class TestController {

    @Autowired
    private TestService testService;

    /**
     * 查询所有数据
     * @return
     */
    @GetMapping("/list")
    @ResponseBody
    public Object getList(){
        List<Test> list = testService.getList();
        return JsonResult.error("错误");

    }

    /**
     * 添加数据
     */

    @GetMapping("/add")
    @ResponseBody
    public void add(){
        testService.insert();
    }

    /**
     * 修改数据
     */
    @GetMapping("/update")
    @ResponseBody
    public void update(){
        testService.update();
    }

    /**
     * 删除数据
     */
    @GetMapping("/delete")
    @ResponseBody
    private void delete(){
        testService.delete(1);
    }

}