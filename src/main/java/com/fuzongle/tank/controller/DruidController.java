package com.fuzongle.tank.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/4 22:54
 * @Description: 数据监控控制层
 */
@Controller
@RequestMapping("/admin/druid")
public class DruidController {

    /**
     * 跳转数据监控
     * @return
     */
    @GetMapping
    public String list() {
        return  "redirect:/druid/index";
    }


}