package com.fuzongle.tank.controller;

import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.service.OpenLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/1 00:35
 * @Description: 操作日志控制层
 */
@Controller
@RequestMapping("/admin/openLog")
public class OpenLogController {

    @Autowired
    private OpenLogService openLogService;

    /**
     *跳转到操作日志列表
     */
    @GetMapping
    public String loginLog() {
        return  "/admin/openLog/list";
    }

    /**
     *操作日志列表数据
     */
    @GetMapping("/list")
    @ResponseBody
    public JsonResult list(Integer page,Integer size,String loginAccount) {
        return openLogService.selectList( page,size, loginAccount);
    }

    /**
     * 操作日志详情数据
     */
    @GetMapping("/details")
    @ResponseBody
    public Object details(@RequestParam Integer id) {
        return JsonResult.success(openLogService.selectById(id));
    }

    /**
     * 删除操作日志
     */
    @GetMapping("/delete")
    @ResponseBody
    public JsonResult delete(@RequestParam Integer id) {
        return  openLogService.delete(id);
    }

}