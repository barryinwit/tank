package com.fuzongle.tank.controller;

import com.fuzongle.tank.annotation.OpenLogAnnotation;
import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.constant.SystemConstant;
import com.fuzongle.tank.entity.Demand;
import com.fuzongle.tank.service.DemandService;
import com.fuzongle.tank.utils.ServletUtils;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/2 23:31
 * @Description: 商机链控制层
 */
@Controller
@RequestMapping("/admin/demand")
public class DemandController {

    @Autowired
    private DemandService demandService;


    /**
     * 跳转需求大厅
     * @return
     */
    @GetMapping
    public String list() {
        return  "/admin/demand/list";
    }

    /**
     * 跳转我发布的列表
     * @return
     */
    @GetMapping("/myList")
    public String myList() {
        return  "/admin/demand/myList";
    }

    /**
     * 跳转我接單列表
     * @return
     */
    @GetMapping("/orderList")
    public String orderList() {
        return  "/admin/demand/orderList";
    }



    @GetMapping("/list")
    @ResponseBody
    public Object list(Integer page, Integer size, String title,Integer type) {

        //获取用户id
        HttpSession session = ServletUtils.getRequestAttributes().getRequest().getSession();
        //获取用户id
        int userId= (int)session.getAttribute("userId");
        Demand demand = new Demand();
        if (type == 2){
            demand.setPublisherId(userId);
        }else  if (type == 3){
            demand.setSnatcherId(userId);
        }
        demand.setTitle(title);
        demand.setType(type);
        List<Demand> userList = demandService.selectList(demand);
        PageInfo<Demand> pageInfo = new PageInfo<>(userList);
        return JsonResult.success(pageInfo.getTotal(),pageInfo.getList());
    }

    /**
     *新增用户反馈
     * @return
     */
    @PostMapping("/add")
    @ResponseBody
    @OpenLogAnnotation(title = "发布需求", ServiceType = SystemConstant.SERVICE_TYPE_ADD)
    public JsonResult add(Demand demand) {
        //获取用户id
        HttpSession session = ServletUtils.getRequestAttributes().getRequest().getSession();
        //获取用户id
        int userId= (int)session.getAttribute("userId");
        demand.setPublisherId(userId);
        demand.setPublisherTime(new Date());
        return  demandService.insert(demand);
    }


    @GetMapping("/details")
    @ResponseBody
    public Object details(@RequestParam Integer id) {
        return JsonResult.success(demandService.selectById(id));
    }

    /**
     * 抢单
     */
    @PostMapping("/snatchOrder")
    @ResponseBody
    @OpenLogAnnotation(title = "抢单", ServiceType = SystemConstant.SERVICE_TYPE_ADD)
    public JsonResult snatchOrder(Integer id,Integer type) {
        //获取用户id
        HttpSession session = ServletUtils.getRequestAttributes().getRequest().getSession();
        //获取用户id
        int userId= (int)session.getAttribute("userId");
        Demand demand = new Demand();
        demand.setId(id);
        if (type == 1){ //抢单
            //查询此订单
            Demand data = demandService.selectById(id);
           if (data.getPublisherId() ==  userId){
                return  JsonResult.error("对不起，自己发布的需求，不能抢单！");
            }
            demand.setStatus(3);//进行中
            demand.setSnatcherId(userId);
            demand.setSnatcherTime(new Date());
        } else if (type == 2){ //下架
            demand.setStatus(1);
        }else if (type == 3){ //上架
            demand.setStatus(2);
        }
        return  demandService.update(demand);
    }

    @GetMapping("/delete")
    @ResponseBody
    @OpenLogAnnotation(title = "删除需求", ServiceType = SystemConstant.SERVICE_TYPE_DELETE)
    public JsonResult delete(@RequestParam Integer id) {
        return  demandService.delete(id);
    }

}