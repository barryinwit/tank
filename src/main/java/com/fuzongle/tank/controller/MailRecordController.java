package com.fuzongle.tank.controller;

import com.fuzongle.tank.annotation.OpenLogAnnotation;
import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.constant.SystemConstant;
import com.fuzongle.tank.entity.MailRecord;
import com.fuzongle.tank.service.MailRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/2 14:07
 * @Description:
 */
@Controller
@RequestMapping("/admin/mailRecord")
public class MailRecordController {

    @Autowired
    private MailRecordService mailRecordService;


    /**
     *跳转到邮件列表页面
     * @return
     */
    @GetMapping
    public String loginLog() {
        return  "/admin/mailRecord/list";
    }

    /**
     * 邮件列表数据
     */
    @GetMapping("/list")
    @ResponseBody
    public JsonResult list(Integer page,Integer size,String title) {
        return mailRecordService.selectList(page,size,title);
    }

    /**
     *邮件详情
     */
    @GetMapping("/details")
    @ResponseBody
    public Object details(@RequestParam Integer id) {
        return JsonResult.success(mailRecordService.selectById(id));
    }

    /**
     *点击添加发送邮件
     * @return
     */
    @PostMapping("/add")
    @ResponseBody
    @OpenLogAnnotation(title = "发送邮件", ServiceType = SystemConstant.SERVICE_TYPE_ADD)
    public JsonResult add(MailRecord mailRecord) {
        return  mailRecordService.insert(mailRecord);
    }

    /**
     * 删除邮件
     */
    @GetMapping("/delete")
    @ResponseBody
    @OpenLogAnnotation(title = "删除邮件", ServiceType = SystemConstant.SERVICE_TYPE_DELETE)
    public JsonResult delete(@RequestParam Integer id) {
        return  mailRecordService.delete(id);
    }
}