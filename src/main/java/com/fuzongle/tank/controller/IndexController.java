package com.fuzongle.tank.controller;

import com.fuzongle.tank.utils.ServletUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpSession;
import java.util.Objects;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/21 14:27
 * @Description:系统首页数据
 */
@Controller
public class IndexController {

    /**
     * 首台主页
     * @return
     */
    @GetMapping("/index")
    public String login(ModelMap map){
        HttpSession session = ServletUtils.getRequestAttributes().getRequest().getSession();
        String account=(String) session.getAttribute("account");
        map.put("account",account);
       if (Objects.isNull(account)){
            return  "login";
        }
        return  "index";
    }

    /**
     * 跳转到首页
     * @return
     */
    @GetMapping("/body")
    public String main(){
        return  "body";
    }

    /**
     * 点击退出系统
     * @return
     */
    @GetMapping("/signOut")
    public String signOut(){
        return  "login";
    }

}