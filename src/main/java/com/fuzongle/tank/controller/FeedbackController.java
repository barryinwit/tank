package com.fuzongle.tank.controller;

import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.entity.Feedback;
import com.fuzongle.tank.service.FeedbackService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/1 09:44
 * @Description: 用户反馈-控制层
 */
@Controller
@RequestMapping("/admin/feedback")
public class FeedbackController {

    @Autowired
    private FeedbackService feedbackService;


    /**
     * 用户列表请求页面
     * @return
     */
    @GetMapping
    public String userList() {
        return  "/admin/feedback/list";
    }

    @GetMapping("/list")
    @ResponseBody
    public Object list(Integer page,Integer size,String title) {
        PageHelper.startPage(page, size);
        List<Feedback> userList = feedbackService.selectList(title);
        PageInfo<Feedback> pageInfo = new PageInfo<>(userList);
        return JsonResult.success(pageInfo.getTotal(),pageInfo.getList());
    }

    /**
     *新增用户反馈
     * @return
     */
    @PostMapping("/add")
    @ResponseBody
    public JsonResult add(Feedback feedback) {
        return  feedbackService.insert(feedback);
    }

    @GetMapping("/details")
    @ResponseBody
    public Object details(@RequestParam Integer id) {
        return JsonResult.success(feedbackService.selectById(id));
    }
}