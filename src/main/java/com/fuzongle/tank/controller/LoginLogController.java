package com.fuzongle.tank.controller;

import com.fuzongle.tank.annotation.OpenLogAnnotation;
import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.constant.SystemConstant;
import com.fuzongle.tank.service.LoginLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/31 21:13
 * @Description:
 */
@Controller
@RequestMapping("/admin/loginLog")
public class LoginLogController {

    @Autowired
    private LoginLogService loginLogService;


    /**
     *跳转到登录列日志列表
     */
    @GetMapping
    public String loginLog() {
        return  "/admin/loginLog/list";
    }

    /**
     * 登录日志列表
     */
    @GetMapping("/list")
    @ResponseBody
    public JsonResult list(Integer page,Integer size,String loginAccount) {
        return loginLogService.selectList(page,size,loginAccount);
    }

    /**
     * 查看详情
     * @param id
     * @return
     */
    @GetMapping("/details")
    @ResponseBody
    public JsonResult details(@RequestParam Integer id) {
        return JsonResult.success(loginLogService.selectById(id));
    }

    /**
     * 删除登录日志
     */
    @GetMapping("/delete")
    @ResponseBody
    @OpenLogAnnotation(title = "删除日志", ServiceType = SystemConstant.SERVICE_TYPE_DELETE)
    public JsonResult delete(@RequestParam Integer id) {
        return  loginLogService.delete(id);
    }

}