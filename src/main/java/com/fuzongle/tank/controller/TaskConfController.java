package com.fuzongle.tank.controller;

import com.fuzongle.tank.annotation.OpenLogAnnotation;
import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.constant.SystemConstant;
import com.fuzongle.tank.entity.TaskConf;
import com.fuzongle.tank.service.TaskConfService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/11 22:40
 * @Description: 任务控制层
 */
@Controller
@RequestMapping("/admin/taskConf")
public class TaskConfController {

    @Autowired
    private TaskConfService taskConfService;

    /**
     *跳转到任务中心列表
     */
    @GetMapping
    public String taskConf() {
        return  "/admin/taskConf/list";
    }

    /**
     * 任务列表数据
     */
    @GetMapping("/list")
    @ResponseBody
    public Object list(Integer page,Integer size,String name) {
        return  taskConfService.selectList(page,size,name);
    }

    /**
     * 新增任务
     */
    @PostMapping("/add")
    @ResponseBody
    @OpenLogAnnotation(title = "添加任务", ServiceType = SystemConstant.SERVICE_TYPE_ADD)
    public JsonResult add(TaskConf taskConf) {
        return  taskConfService.insert(taskConf);
    }


    /**
     * 查看任务详情
     */
    @GetMapping("/details")
    @ResponseBody
    public JsonResult edit(@RequestParam Integer id) {
        return  taskConfService.selectById(id);
    }

    /**
     * 修改任务保存
     */
    @PostMapping("/edit")
    @ResponseBody
    @OpenLogAnnotation(title = "编辑任务", ServiceType = SystemConstant.SERVICE_TYPE_EDIT)
    public JsonResult edit(TaskConf taskConf) {
        return  taskConfService.update(taskConf);
    }

    /**
     * 开启或者关闭任务
     */
    @PostMapping("/openStatus")
    @ResponseBody
    public JsonResult openStatus(TaskConf taskConf) {
        return  taskConfService.openStatus(taskConf);
    }

    /**
     * 删除任务
     */
    @GetMapping("/delete")
    @ResponseBody
    @OpenLogAnnotation(title = "删除任务", ServiceType = SystemConstant.SERVICE_TYPE_DELETE)
    public JsonResult delete(@RequestParam Integer id) {
        return  taskConfService.delete(id);
    }
}