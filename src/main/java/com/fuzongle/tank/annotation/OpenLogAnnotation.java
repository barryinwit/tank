package com.fuzongle.tank.annotation;


import java.lang.annotation.*;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/24 14:01
 * @Description: 自定义注解：操作日志
 */
@Target({ ElementType.PARAMETER, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface OpenLogAnnotation {
    /**
     * 操作标题
     * @return
     */
    String title() default "";

    /**
     * 业务类型 默认0 其他的
     * @return
     */
    int ServiceType() default 0;
}