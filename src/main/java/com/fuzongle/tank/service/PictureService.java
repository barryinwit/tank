package com.fuzongle.tank.service;

import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.entity.Picture;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/12 23:34
 * @Description: 图片上传与下载-业务层接口类
 */
public interface PictureService {

   JsonResult selectList(Integer page,Integer size,String name);

    JsonResult upload(MultipartFile multipartFile, HttpServletRequest request);

    JsonResult insert(Picture picture);

    void download(Integer id, HttpServletRequest request, HttpServletResponse response) throws Exception;

    JsonResult delete(Integer id);
}