package com.fuzongle.tank.service.impl;

import com.fuzongle.tank.base.BaseService;
import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.dao.TaskConfDao;
import com.fuzongle.tank.entity.TaskConf;
import com.fuzongle.tank.service.TaskConfService;
import com.fuzongle.tank.test.Test;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/11 22:31
 * @Description: 任务配置业务层实现类
 */
@Service
public class TaskConfServiceImpl extends BaseService implements TaskConfService {

    @Autowired
    private TaskConfDao taskConfDao;

    @Override
    public JsonResult selectList(Integer page,Integer size,String name) {
        PageHelper.startPage(page, size, getOrderBy("create_time","desc"));
        List<TaskConf> taskConfList = taskConfDao.selectList(name);
        PageInfo<TaskConf> pageInfo = new PageInfo<>(taskConfList);
        return JsonResult.success(pageInfo.getTotal(),pageInfo.getList());
    }

    @Override
    public JsonResult insert(TaskConf taskConf) {
        taskConf.setCreateTime(new Date());
        taskConf.setUpdateTime(new Date());
        int a=taskConfDao.insert(taskConf);
        if (a>0){
            return  JsonResult.success("添加成功!");
        }
        return  JsonResult.error("添加失败!");
    }

    @Override
    public JsonResult update(TaskConf taskConf) {
        try {
            Test.modifyJobTimeTrue(taskConf.getName(),"0","0","0",Class.forName(taskConf.getClassName()),taskConf.getCron());
        }catch (Exception e){
            System.out.println("异常");
        }
        int rows = taskConfDao.update(taskConf);
        if (rows>0){
            return JsonResult.success();
        }
        return JsonResult.error();
    }

    @Override
    public JsonResult selectById(Integer id) {
        return JsonResult.success(taskConfDao.selectById(id));
    }

    @Override
    public JsonResult openStatus(TaskConf taskConf) {
        //查询任务数据
        TaskConf taskData=taskConfDao.selectById(taskConf.getId());
        if (taskConf.getStatus() == 1){//开启任务
            try {
                Test.addJob(taskData.getName() ,"0",
                        "0", "0",
                        Class.forName(taskData.getClassName()), taskData.getCron());
            }catch (Exception e){
                System.out.println("异常");
            }
        }
        if (taskConf.getStatus() == 2){//关闭任务
            Test.removeJob(taskData.getName(),"0","0","0");
        }
        int rows = taskConfDao.updateStatus(taskConf);
        if (rows>0){
            return JsonResult.success();
        }
        return JsonResult.error();
    }

    @Override
    public JsonResult delete(Integer id) {
        int rows=0;
         rows=taskConfDao.delete(id);
         if (rows>0){
             return JsonResult.success();
         }
        return JsonResult.error();
    }
}