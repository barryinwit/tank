package com.fuzongle.tank.service.impl;

import com.fuzongle.tank.base.BaseService;
import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.dao.OpenLogDao;
import com.fuzongle.tank.entity.OpenLog;
import com.fuzongle.tank.service.OpenLogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/26 00:55
 * @Description: 操作日志业务层实现类
 */
@Service
public class OpenLogServiceImpl extends BaseService implements OpenLogService {

    @Autowired
    private OpenLogDao openLogDao;


    @Override
    public void insert(OpenLog openLog) {
        openLogDao.insert(openLog);
    }

    @Override
    public JsonResult selectList(Integer page,Integer size,String title) {
        PageHelper.startPage(page, size,getOrderBy("open_time","desc"));
        List<OpenLog> openLogList = openLogDao.selectList(title);
        PageInfo<OpenLog> pageInfo = new PageInfo<>(openLogList);
        return JsonResult.success(pageInfo.getTotal(),pageInfo.getList());
    }

    @Override
    public OpenLog selectById(Integer id) {
        return openLogDao.selectById(id);
    }

    @Override
    public JsonResult delete(Integer id) {
        int rows=0;
        rows =openLogDao.delete(id);
        if (rows>0){
            return JsonResult.success() ;
        }
        return JsonResult.error() ;
    }
}