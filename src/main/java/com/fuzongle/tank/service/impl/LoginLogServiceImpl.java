package com.fuzongle.tank.service.impl;

import com.fuzongle.tank.base.BaseService;
import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.dao.LoginLogDao;
import com.fuzongle.tank.entity.LoginLog;
import com.fuzongle.tank.service.LoginLogService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/24 12:54
 * @Description:
 */

@Service
public class LoginLogServiceImpl  extends BaseService implements LoginLogService {

    @Autowired
    private LoginLogDao loginLogDao;


    @Override
    public void insert(LoginLog loginLog) {
        loginLogDao.insert(loginLog);
    }

    @Override
    public JsonResult selectList(Integer page, Integer size, String loginAccount) {
        //开始分页
        PageHelper.startPage(page, size,getOrderBy("login_time","desc"));
        //查询登录日志
        List<LoginLog> userList = loginLogDao.selectList(loginAccount);
        PageInfo<LoginLog> pageInfo = new PageInfo<>(userList);
        return JsonResult.success(pageInfo.getTotal(),pageInfo.getList());
    }

    @Override
    public LoginLog selectById(Integer id) {
        return loginLogDao.selectById(id);
    }

    @Override
    public JsonResult delete(Integer id) {
        int rows = 0;
        rows = loginLogDao.delete(id);
        if (rows>0){
            return  JsonResult.success();
        }
        return JsonResult.error();
    }
}