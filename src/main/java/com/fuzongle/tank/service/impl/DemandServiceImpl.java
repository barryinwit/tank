package com.fuzongle.tank.service.impl;

import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.dao.DemandDao;
import com.fuzongle.tank.entity.Demand;
import com.fuzongle.tank.service.DemandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/2 23:36
 * @Description: 需求业务层实现
 */
@Service
public class DemandServiceImpl implements DemandService {

    @Autowired
    private DemandDao demandDao;

    @Override
    public List<Demand> selectList(Demand demand) {
        return demandDao.selectList(demand);
    }

    @Override
    public JsonResult insert(Demand demand) {
        int insert = demandDao.insert(demand);
        if (insert>0){
            return JsonResult.success();
        }
        return JsonResult.error();
    }

    @Override
    public Demand selectById(Integer id) {
        return demandDao.selectById(id);
    }

    @Override
    public JsonResult update(Demand demand) {
         int update = demandDao.update(demand);
        return JsonResult.success();
    }

    @Override
    public JsonResult delete(Integer id) {
         int a=demandDao.delete(id);
        return JsonResult.error("删除成功");
    }
}