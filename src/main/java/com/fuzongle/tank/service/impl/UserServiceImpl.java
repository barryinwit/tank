package com.fuzongle.tank.service.impl;

import com.fuzongle.tank.base.BaseService;
import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.dao.UserDao;
import com.fuzongle.tank.entity.LoginLog;
import com.fuzongle.tank.entity.User;
import com.fuzongle.tank.service.LoginLogService;
import com.fuzongle.tank.service.UserService;
import com.fuzongle.tank.utils.*;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/21 00:56
 * @Description: 用户业务层实现类
 */
@Service
public class UserServiceImpl extends BaseService implements UserService  {

    @Autowired
    private UserDao userDao; //注入用户数据层

    @Autowired
    private LoginLogService loginLogService; //注入登录日志service 注意：不能跨当前Dao 直接调用方法

    /**
     * 登录业务层
     * @param account
     * @param password
     * @return
     */
    @Override
    @Transactional //事务注解
    public JsonResult login(String account, String password) {

        if (account == null){//判断帐号是否为空
            return JsonResult.error("您输入的账号不能为空");
        }
        if (password == null){//判断密码是否为空
            return JsonResult.error("您输入的密码不能为空");
        }

        //获取request对象
        HttpServletRequest request = ServletUtils.getRequest();
        //获取ip
        String ip = IpUtils.getIpAddress(request);
        //通过ip获取位置
        String realAddressByIP = AddressUtils.getRealAddressByIP(ip);
        //获取访问浏览器
        String browserName = BrowserUtils.getBrowserName(request);
        //操作系统
        String osName = BrowserUtils.getOsName(request);

        //创建登录日志对象
        LoginLog loginLog = new LoginLog();
        loginLog.setLoginAccount(account);//帐号
        loginLog.setLoginTime(new Date());//登录时间
        loginLog.setIp(ip);//获取登陆人ip
        loginLog.setLoginPlace(realAddressByIP);//登录位置
        loginLog.setBrowser(browserName);//浏览器名字
        loginLog.setOperatingSystem(osName);//操作系统

        //查询帐号是否存在
        User user = userDao.selectByAccount(account);
        if (user == null){
            loginLog.setStatus(2);
            loginLog.setInfo("帐号不存在");
            loginLogService.insert(loginLog);//保存登录信息
            return JsonResult.error("您输入的帐号不存在，请重新输入！");
        }
        //获取数据库用户的密码
        String dataPassword = user.getPassword();

        //将用户输入密码密码加密
        String md5Str = MD5Utils.getMD5Str(password);
        if (!md5Str.equals(dataPassword)){//判断数据库的密码和用户输入的是否一致
            loginLog.setStatus(2);
            loginLog.setInfo("密码错误");
            loginLogService.insert(loginLog);//保存登录信息
            return JsonResult.error("您输入的密码不正确请重新输入！");
        }
        loginLog.setStatus(1);
        loginLog.setInfo("登录成功");
        loginLogService.insert(loginLog);//保存登录信息

        //添加到session
        HttpSession session = request.getSession();
        session.setAttribute("account",account);
        session.setAttribute("userId",user.getId());
        //session过期时间设置，以秒为单位，即在没有活动30分钟后，session将失效
        session.setMaxInactiveInterval(30 * 60);

        return JsonResult.success();
    }

    /**
     * 用户列表
     * @param account
     * @param name
     * @param page
     * @param size
     * @return
     */
    @Override
    public JsonResult selectList(String account, String name,Integer page, Integer size){
        //开始分页
        PageHelper.startPage(page, size, getOrderBy("create_time","desc"));
        //查询用户列表
        List<User> uList = userDao.selectList(account, name);
        //分页
        PageInfo<User> pageInfo = new PageInfo<User>(uList);
        //获取分页数据
        return JsonResult.success(pageInfo.getTotal(),pageInfo.getList());
    }

    @Override
    public JsonResult insert(User user) {

        if (user.getAccount() == null){
            return JsonResult.error("帐号不能为空！");
        }else if(user.getPassword() == null) {
            return JsonResult.error("密码不能为空！");
        }else if(user.getName() == null){
            return JsonResult.error("名称不能为空！");
        }

        //查询数据帐号是否存在
        User userData = userDao.selectByAccount(user.getAccount());
        if (Objects.nonNull(userData)){
            return JsonResult.error("该用户已存在，请重新输入！");
        }

        user.setPassword(MD5Utils.getMD5Str(user.getPassword()));
        user.setCreateTime(new Date());
        //保存用户数据
        int insert = userDao.insert(user);

        if (insert>0){
        return  JsonResult.success();
        }
        return JsonResult.error();
    }

    @Override
    public JsonResult update(User user) {
        String md5Str = MD5Utils.getMD5Str(user.getPassword());
        user.setPassword(md5Str);
        int update = userDao.update(user);
        if (update>0){
            return  JsonResult.success();
        }
        return  JsonResult.error();
    }

    @Override
    public User selectById(Integer id) {
        return userDao.selectById(id);
    }

    @Override
    public JsonResult delete(Integer id) {
        //通过id删除用户
        int delete=userDao.deleteById(id);
        if (delete>0){
            return  JsonResult.success();
        }
        return  JsonResult.error();
    }




}