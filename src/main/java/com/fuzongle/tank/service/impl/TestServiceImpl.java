package com.fuzongle.tank.service.impl;

import com.fuzongle.tank.dao.TestDao;
import com.fuzongle.tank.entity.Test;
import com.fuzongle.tank.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/17 12:43
 * @Description:测试业务实现类
 */
@Service
public class TestServiceImpl implements TestService {
    @Autowired
    private TestDao testDao;


    @Override
    public List<Test> getList() {
        return testDao.getList();
    }

    @Override
    @Transactional
    public void insert() {
        Test test = new Test();
        test.setName("测试数据");
        test.setAge(11);
        test.setCreateTime(new Date());
        testDao.insert(test);
    }

    @Override
    public void update() {
        Test test = new Test();
        test.setId(1);
        test.setName("测试修改数据");
        test.setAge(11);
        test.setCreateTime(new Date());
        testDao.update(test);
    }


    @Override
    public void delete(Integer id) {
        testDao.delete(id);
    }


}