package com.fuzongle.tank.service.impl;

import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.dao.FeedbackDao;
import com.fuzongle.tank.entity.Feedback;
import com.fuzongle.tank.service.FeedbackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/1 09:43
 * @Description: 用户反馈 业务层实现类
 */
@Service
public class FeedbackServiceImpl implements FeedbackService {

    @Autowired
    private FeedbackDao feedbackDao;

    @Override
    public List<Feedback> selectList(String title) {
        return feedbackDao.selectList(title);
    }

    @Override
    public JsonResult insert(Feedback feedback) {
        feedback.setCreateTime(new Date());
        int insert = feedbackDao.insert(feedback);
        if (insert>0){
            return  JsonResult.success();
        }
        return  JsonResult.error();
    }

    @Override
    public Feedback selectById(Integer id) {
        return feedbackDao.selectById(id);
    }
}