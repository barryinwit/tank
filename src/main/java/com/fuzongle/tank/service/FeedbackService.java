package com.fuzongle.tank.service;

import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.entity.Feedback;

import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/1 09:42
 * @Description: 用户反馈业务层接口类
 */
public interface FeedbackService {


    List<Feedback> selectList(String title);

    JsonResult insert(Feedback feedback);

    Feedback selectById(Integer id);
}