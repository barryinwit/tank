package com.fuzongle.tank.service;

import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.entity.Demand;

import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/2 23:36
 * @Description: 需求业务层接口类
 */
public interface DemandService {


    List<Demand> selectList(Demand demand);

    JsonResult insert(Demand demand);

    Demand selectById(Integer id);


    JsonResult update(Demand demand);

    JsonResult delete(Integer id);
}