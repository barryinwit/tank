package com.fuzongle.tank.service;

import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.entity.User;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/21 00:56
 * @Description: 用户业务层接口类
 */
public interface UserService {

    JsonResult login(String account, String password);

    JsonResult selectList(String account,String name, Integer page,Integer size);

    JsonResult insert(User user);

    JsonResult update(User user);

    User selectById(Integer id);

    JsonResult delete(Integer id);
}