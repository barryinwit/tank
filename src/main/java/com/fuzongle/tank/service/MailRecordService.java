package com.fuzongle.tank.service;

import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.entity.MailRecord;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/2 14:06
 * @Description:邮件记录业务层接口类
 */
public interface MailRecordService {
    JsonResult selectList(Integer page,Integer size,String title);

    MailRecord selectById(Integer id);

    JsonResult insert(MailRecord mailRecord);

    JsonResult delete(Integer id);
}