package com.fuzongle.tank.service;

import com.fuzongle.tank.entity.Test;

import java.util.List;

/**
 * 测试业务接口类
 */
public interface TestService {

    List<Test> getList();

    void insert();

    void update();

    void delete(Integer id);
}
