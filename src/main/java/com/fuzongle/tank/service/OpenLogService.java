package com.fuzongle.tank.service;

import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.entity.OpenLog;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/26 00:55
 * @Description: 操作日志业务层接口类
 */
public interface OpenLogService {
    void insert(OpenLog openLog);

    JsonResult selectList(Integer page,Integer size,String loginAccount);

    OpenLog selectById(Integer id);

    JsonResult delete(Integer id);
}