package com.fuzongle.tank.service;

import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.entity.LoginLog;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/24 12:53
 * @Description:
 */
public interface LoginLogService {

    void insert(LoginLog loginLog);

    JsonResult selectList(Integer page, Integer size, String loginAccount);

    LoginLog selectById(Integer id);

    JsonResult delete(Integer id);
}