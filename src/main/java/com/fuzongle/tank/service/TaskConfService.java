package com.fuzongle.tank.service;

import com.fuzongle.tank.base.JsonResult;
import com.fuzongle.tank.entity.TaskConf;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/11 22:30
 * @Description: 任务业务层接口类
 */
public interface TaskConfService  {

    JsonResult selectList(Integer page,Integer size,String name);

    JsonResult insert(TaskConf taskConf);

    JsonResult update(TaskConf taskConf);

    JsonResult selectById(Integer id);

    JsonResult openStatus(TaskConf taskConf);

    JsonResult delete(Integer id);
}