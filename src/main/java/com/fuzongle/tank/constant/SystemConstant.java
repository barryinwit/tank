package com.fuzongle.tank.constant;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/24 14:09
 * @Description: 操作日志相关常量
 */
public class SystemConstant {

    private SystemConstant() {}

    /**
     * 操作日志类型：添加
     */
    public static final int SERVICE_TYPE_ADD = 1;
    /**
     * 操作日志类型：删除
     */
    public static final int SERVICE_TYPE_DELETE = 2;
    /**
     * 操作日志类型：修改
     */
    public static final int SERVICE_TYPE_EDIT= 3;


    /**
     * 返回值常量
     */
    public static final String CODE_TAG = "code";

    public static final String MSG_TAG = "msg";

    public static final String DATA_TAG = "data";

    public static final String TOTAL_TAG = "total";

    public static final String SUCCESS = "操作成功";

    public static final String ERROR = "操作失败";



}