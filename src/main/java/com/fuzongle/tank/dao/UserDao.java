package com.fuzongle.tank.dao;

import com.fuzongle.tank.entity.User;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/21 00:57
 * @Description: 用户数据层
 */
public interface UserDao {

    /**
     * 通过登录帐号查询用户
     * @param account
     * @return
     */
    User selectByAccount(String account);

    List<User> selectList(@Param("account") String account, @Param("name") String name);


    int insert(User user);

    int update(User user);

    User selectById(Integer id);

    int deleteById(Integer id);
}