package com.fuzongle.tank.dao;

import com.fuzongle.tank.entity.Demand;

import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/2 23:35
 * @Description: 需求数据层
 */
public interface DemandDao {


    List<Demand> selectList( Demand demand);


    int insert(Demand demand);

    Demand selectById(Integer id);


    int update(Demand demand);

    int delete(Integer id);
}