package com.fuzongle.tank.dao;

import com.fuzongle.tank.entity.Feedback;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/1 09:42
 * @Description: 用户反馈-数据层
 */
public interface FeedbackDao {

    List<Feedback> selectList(@Param("title") String title);

    int insert(Feedback feedback);

    int update(Feedback feedback);

    int deleteById(Integer id);

    Feedback selectById(Integer id);






}