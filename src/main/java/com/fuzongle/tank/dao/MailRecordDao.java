package com.fuzongle.tank.dao;

import com.fuzongle.tank.entity.MailRecord;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/2 13:24
 * @Description: 邮件记录Dto
 */
public interface MailRecordDao {

    int insert(MailRecord mailRecord);


    List<MailRecord> selectList(@Param("title") String title);


    MailRecord selectById(Integer id);


    int delete(Integer id);
}