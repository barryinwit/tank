package com.fuzongle.tank.dao;

import com.fuzongle.tank.entity.TaskConf;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/11 22:28
 * @Description: 任务数据层接口
 */
public interface TaskConfDao {


    List<TaskConf> selectList(@Param("name") String name);


    int insert(TaskConf taskConf);

    int update(TaskConf taskConf);
    int updateStatus(TaskConf taskConf);

    TaskConf selectById(@Param("id") int id);

    int delete(Integer id);
}