package com.fuzongle.tank.dao;

import com.fuzongle.tank.entity.Test;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/17 12:43
 * @Description: 测试数据层
 */
@Repository
public interface TestDao {

    List<Test> getList();


    void insert(Test test);

    void update(Test test);

    void delete(Integer id);
}