package com.fuzongle.tank.dao;

import com.fuzongle.tank.entity.LoginLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/24 12:54
 * @Description:
 */
public interface LoginLogDao {

    void insert(LoginLog loginLog);

    List<LoginLog> selectList(@Param("loginAccount") String loginAccount);

    LoginLog selectById(Integer id);

    int delete(Integer id);
}