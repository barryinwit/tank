package com.fuzongle.tank.dao;

import com.fuzongle.tank.entity.Picture;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/12 23:35
 * @Description: 图片上传与下载数据层
 */
public interface PictureDao {

  List<Picture> selectList(@Param("name") String name);


  int insert(Picture picture);

  int delete(Integer id);
}