package com.fuzongle.tank.dao;

import com.fuzongle.tank.entity.OpenLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/26 00:53
 * @Description: 操作日志数据层
 */
public interface OpenLogDao {
    void insert(OpenLog openLog);

    List<OpenLog> selectList(@Param("title") String title);

    OpenLog selectById(Integer id);

    int delete(Integer id);
}