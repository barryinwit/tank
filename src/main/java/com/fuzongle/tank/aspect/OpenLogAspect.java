package com.fuzongle.tank.aspect;

import com.fuzongle.tank.annotation.OpenLogAnnotation;
import com.fuzongle.tank.entity.OpenLog;
import com.fuzongle.tank.service.OpenLogService;
import com.fuzongle.tank.utils.AddressUtils;
import com.fuzongle.tank.utils.BrowserUtils;
import com.fuzongle.tank.utils.ServletUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/24 14:22
 * @Description: 操作日志AOP处理方法
 */
@Aspect
@Component
public class OpenLogAspect {

    @Autowired
    private OpenLogService openLogService;

    private static final Logger logger = LoggerFactory.getLogger(OpenLogAspect.class);

    @Pointcut("@annotation(com.fuzongle.tank.annotation.OpenLogAnnotation)")
    public void logAop() {
        System.out.println("切入点...");
    }

    /**
     * 方法调用后触发 , 记录正常操作
     *
     * @param joinPoint
     * @throws ClassNotFoundException
     */
    @AfterReturning("logAop()")
    public  void after(JoinPoint joinPoint) throws ClassNotFoundException {
        serviceLog(joinPoint,null);
    }
    /**
     *  发生异常，走此方法
     * @param joinPoint
     * @param e
     */
    @AfterThrowing(pointcut = "logAop()", throwing = "e")
    public void AfterThrowing(JoinPoint joinPoint, Throwable e) {
        serviceLog(joinPoint,e);
    }


    /**
     * 逻辑处理方法
     * @param joinPoint
     * @param e
     */
    public void serviceLog(JoinPoint joinPoint,Throwable e){
        MethodSignature methodSignature = (MethodSignature) joinPoint.getSignature();
        //获取注解
        Method method = methodSignature.getMethod();
        //判断注解是否为空
        if (method == null){
            return;
        }
        //类名
        String className = joinPoint.getTarget().getClass().getName();
        //方法名
        String methodName = joinPoint.getSignature().getName();
        //获取request对象
        HttpServletRequest request = ServletUtils.getRequestAttributes().getRequest();
        //请求地址
        String requestURI = request.getRequestURI();
        //请求方法
        String requestMethod = request.getMethod();
        //ip
        String remoteAddr = request.getRemoteAddr();
        //获取方法上面的注解
        OpenLogAnnotation openLogAnnotation  = method.getAnnotation(OpenLogAnnotation.class);
        OpenLog openLog = new OpenLog();
        openLog.setTitle(openLogAnnotation.title()); //设置标题
        openLog.setServiceType(openLogAnnotation.ServiceType());//业务类型
        openLog.setPackageName(className);//包名
        openLog.setMethodName(methodName);//方法名字
        openLog.setRequestMode(requestMethod); //请求方式
        openLog.setRequestUrl(requestURI);//请求地址
        openLog.setRequestIp(remoteAddr);//请求ip
        openLog.setRequestPlace(AddressUtils.getRealAddressByIP(remoteAddr));//获取地点
        openLog.setRequestBrowser(BrowserUtils.getBrowserName(request));
        openLog.setStatus(1);
        openLog.setOpenName("xxx");
        openLog.setOpenTime(new Date());
        openLogService.insert(openLog);
    }




}