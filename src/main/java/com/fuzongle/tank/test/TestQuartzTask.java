package com.fuzongle.tank.test;

import com.fuzongle.tank.aspect.OpenLogAspect;
import org.quartz.InterruptableJob;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.UnableToInterruptJobException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestQuartzTask implements InterruptableJob {

	private boolean _interrupted = false;
	private static final Logger logger = LoggerFactory.getLogger(OpenLogAspect.class);



	@Override
	public void execute(JobExecutionContext arg0) throws JobExecutionException {
		System.out.println("aa");
		logger.info("成功执行");
		try {
			Test.addJob("quartzList.get(i).getJobname()", "quartzList.get(i).getJobgroup()",
					"quartzList.get(i).getTriggername()", "quartzList.get(i).getTriggergroup()",
					Class.forName("com.fuzongle.tank.test.TestQuartzTask"), "*/5 * * * * ?");
		}catch (Exception e){

		}
		Test.removeJob("quartzList.get(i).getJobname()");
		_interrupted = true;
	}

	@Override
	public void interrupt() throws UnableToInterruptJobException {
		System.out.println("【关闭】interrupt执行立刻停止:test定时发送...");
		_interrupted = true;
	}
}
