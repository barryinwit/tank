package com.fuzongle.tank.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/24 01:17
 * @Description: 登录日志实体类：login_log
 */
public class LoginLog {

    private Integer id;
    /**
     * '登录帐号'
     */
    private String loginAccount;
    /**
     * 登录ip
     */
    private String ip;
    /**
     * 登录浏览器
     */
    private String browser;

    /**
     *登录地点
     */
    private String loginPlace;

    /**
     * 操作系统
     */
    private String operatingSystem;

    /**
     * 登录状态：1正常 2失败
     */
    private Integer status;
    /**
     * 登录信息
     */
    private String info;

    /**
     * 登录时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private Date loginTime;


    //get set 方法
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLoginAccount() {
        return loginAccount;
    }

    public void setLoginAccount(String loginAccount) {
        this.loginAccount = loginAccount;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getBrowser() {
        return browser;
    }

    public void setBrowser(String browser) {
        this.browser = browser;
    }

    public String getLoginPlace() {
        return loginPlace;
    }

    public void setLoginPlace(String loginPlace) {
        this.loginPlace = loginPlace;
    }

    public String getOperatingSystem() {
        return operatingSystem;
    }

    public void setOperatingSystem(String operatingSystem) {
        this.operatingSystem = operatingSystem;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Date getLoginTime() {
        return loginTime;
    }

    public void setLoginTime(Date loginTime) {
        this.loginTime = loginTime;
    }

    //toString 方法


    @Override
    public String toString() {
        return "LoginLog{" +
                "id=" + id +
                ", loginAccount='" + loginAccount + '\'' +
                ", ip='" + ip + '\'' +
                ", browser='" + browser + '\'' +
                ", loginPlace='" + loginPlace + '\'' +
                ", operatingSystem='" + operatingSystem + '\'' +
                ", status='" + status + '\'' +
                ", info='" + info + '\'' +
                ", loginTime=" + loginTime +
                '}';
    }
}