package com.fuzongle.tank.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * @Auther: fuzongle
 * @Date: 2020/5/25 20:55
 * @Description: 操作日志 实体类：open_log
 */
public class OpenLog {

    /**
     * 操作id
     */
    private Integer id;

    /**
     * 操作标题
     */
    private String title;

    /**
     *操作类型： 0 其他 1添加 2删除 3修改
     */
    private Integer serviceType;

    /**
     *包名字
     */
    private String packageName;

    /**
     * 方法名字
     */
    private String methodName;

    /**
     *请求方式
     */
    private String requestMode;

    /**
     * 请求地址
     */
    private String requestUrl;

    /**
     *请求IP
     */
    private String requestIp;

    /**
     * 请求状态 1正常 2异常
     */
    private Integer status;

    /**
     * 操作人名字
     */
    private String openName;

    /**
     * 操作时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private Date openTime;

    private String requestBrowser;

    private String requestPlace;

    public String getRequestBrowser() {
        return requestBrowser;
    }

    public void setRequestBrowser(String requestBrowser) {
        this.requestBrowser = requestBrowser;
    }

    public String getRequestPlace() {
        return requestPlace;
    }

    public void setRequestPlace(String requestPlace) {
        this.requestPlace = requestPlace;
    }

    //get set 方法
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getServiceType() {
        return serviceType;
    }

    public void setServiceType(Integer serviceType) {
        this.serviceType = serviceType;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public String getRequestMode() {
        return requestMode;
    }

    public void setRequestMode(String requestMode) {
        this.requestMode = requestMode;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public String getRequestIp() {
        return requestIp;
    }

    public void setRequestIp(String requestIp) {
        this.requestIp = requestIp;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getOpenName() {
        return openName;
    }

    public void setOpenName(String openName) {
        this.openName = openName;
    }

    public Date getOpenTime() {
        return openTime;
    }

    public void setOpenTime(Date openTime) {
        this.openTime = openTime;
    }

    //ToString 方法
    @Override
    public String toString() {
        return "OpenLog{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", serviceType=" + serviceType +
                ", packageName='" + packageName + '\'' +
                ", methodName='" + methodName + '\'' +
                ", requestMode='" + requestMode + '\'' +
                ", requestUrl='" + requestUrl + '\'' +
                ", requestIp='" + requestIp + '\'' +
                ", status=" + status +
                ", openName='" + openName + '\'' +
                ", openTime=" + openTime +
                '}';
    }
}