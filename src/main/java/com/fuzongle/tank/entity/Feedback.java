package com.fuzongle.tank.entity;

import java.util.Date;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/1 09:37
 * @Description: 实体类：反馈表 →feedback
 */
public class Feedback {

    /**
     * 反馈id
     */
    private  Integer id;

    /**
     * 标题
     */
    private String title;
    /**
     * 内容
     */
    private String content;

    /**
     * 反馈人id
     */
    private Integer userId;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     *联系电话
     */
    private String contactNumber;

    /**
     * 联系人
     */
    private String contactName;

    /**
     * 1未解决 2解决
     * @return
     */
    private Integer status;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    @Override
    public String toString() {
        return "Feedback{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", userId=" + userId +
                ", createTime=" + createTime +
                ", contactNumber='" + contactNumber + '\'' +
                ", contactName='" + contactName + '\'' +
                '}';
    }

}