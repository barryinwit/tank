package com.fuzongle.tank.entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.Date;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/1 14:11
 * @Description: 需求实体类：demand
 */
public class Demand {

    private Integer id;
    /**
     * '需求标题'
     */
    private String title;
    /**
     *需求类型ID
     */
    private Integer demandTypeId;

    /**
     * 需求描述
     */
    private String demandDescribe;

    /**
     * 技术要求
     */
    private String technology;

    /**
     * 联系人姓名
     */
    private String contactName;

    /**
     *联系人手机号
     */
    private String contactNumber;

    /**
     * 预计时长
     */
    private Integer estimatedDuration;

    /**
     * 赏金
     */
    private Double moneyReward;

    /**
     * 发布人id
     */
    private Integer publisherId;

    /**
     * 发布时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private Date publisherTime;


    /**
     * 抢单人id
     */
    private Integer snatcherId;

    /**
     * 抢单时间
     */
    @JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
    private Date snatcherTime;

    /**
     * 判断是否修改
     */
    private Integer type;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * 1.待发布  2.待接单 3进行中 4已完成
     */
    private Integer status;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getDemandTypeId() {
        return demandTypeId;
    }

    public void setDemandTypeId(Integer demandTypeId) {
        this.demandTypeId = demandTypeId;
    }

    public String getDemandDescribe() {
        return demandDescribe;
    }

    public void setDemandDescribe(String demandDescribe) {
        this.demandDescribe = demandDescribe;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public Integer getEstimatedDuration() {
        return estimatedDuration;
    }

    public void setEstimatedDuration(Integer estimatedDuration) {
        this.estimatedDuration = estimatedDuration;
    }

    public Double getMoneyReward() {
        return moneyReward;
    }

    public void setMoneyReward(Double moneyReward) {
        this.moneyReward = moneyReward;
    }

    public Integer getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(Integer publisherId) {
        this.publisherId = publisherId;
    }

    public Date getPublisherTime() {
        return publisherTime;
    }

    public void setPublisherTime(Date publisherTime) {
        this.publisherTime = publisherTime;
    }

    public Integer getSnatcherId() {
        return snatcherId;
    }

    public void setSnatcherId(Integer snatcherId) {
        this.snatcherId = snatcherId;
    }

    public Date getSnatcherTime() {
        return snatcherTime;
    }

    public void setSnatcherTime(Date snatcherTime) {
        this.snatcherTime = snatcherTime;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Demand{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", demandTypeId=" + demandTypeId +
                ", demandDescribe='" + demandDescribe + '\'' +
                ", technology='" + technology + '\'' +
                ", contactName='" + contactName + '\'' +
                ", contactNumber='" + contactNumber + '\'' +
                ", estimatedDuration=" + estimatedDuration +
                ", moneyReward='" + moneyReward + '\'' +
                ", publisherId=" + publisherId +
                ", publisherTime=" + publisherTime +
                ", snatcherId=" + snatcherId +
                ", snatcherTime='" + snatcherTime + '\'' +
                ", status=" + status +
                '}';
    }
}