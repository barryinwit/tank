package com.fuzongle.tank.task;

import org.quartz.InterruptableJob;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.UnableToInterruptJobException;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/11 22:57
 * @Description: 测试任务执行方法
 */
public class TestTask implements InterruptableJob {

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        System.out.println("开始执行");

    }

    @Override
    public void interrupt() throws UnableToInterruptJobException {
        System.out.println("【关闭】interrupt执行立刻停止:test定时发送...");
    }


}