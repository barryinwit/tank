package com.fuzongle.tank.base;


import com.fuzongle.tank.constant.SystemConstant;

import java.util.HashMap;
import java.util.Objects;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/2 17:42
 * @Description: json 返回數據封裝
 */

public class JsonResult extends  HashMap<String, Object> {

    /**
     * 状态码
     */
    private int code;

    /**
     * 消息
     */
    private String msg;

    /**
     * 数据对象
     */
    private Object data;

    /**
     * 总条数
     */
    private long total;

    /**
     * 枚举类型
     */
    private ResultType type;

    public static JsonResult success() {
        return JsonResult.success(SystemConstant.SUCCESS);
    }
    public static JsonResult success(Object data) {
        return JsonResult.success(SystemConstant.SUCCESS, data);
    }
    public static JsonResult success(String msg) {
        return JsonResult.success(msg, null);
    }

    public static JsonResult success(long total, Object data) {
        return JsonResult.success(SystemConstant.SUCCESS,total, data);
    }
    public static JsonResult success(String msg, long total, Object data) {
        return new JsonResult(ResultType.SUCCESS, msg,total ,data);
    }

    public static JsonResult success(String msg, Object data) {
        return new JsonResult(ResultType.SUCCESS, msg, data);
    }

    public static JsonResult error() {
        return JsonResult.error(SystemConstant.ERROR);
    }
    public static JsonResult error(String msg) {
        return JsonResult.error(msg, null);
    }

    public static JsonResult error(String msg, Object data) {
        return new JsonResult(ResultType.ERROR, msg, data);
    }

    public ResultType getType() {
        return type;
    }

    public void setType(ResultType type) {
        this.type = type;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public long getTotal() {
        return total;
    }
    public void setTotal(long total) {
        this.total = total;
    }


    public JsonResult() {
    }
    public JsonResult(ResultType type, String msg) {
        super.put(SystemConstant.CODE_TAG, type.value());
        super.put(SystemConstant.MSG_TAG, msg);
    }

    public JsonResult(ResultType type, String msg, Object data) {
        super.put(SystemConstant.CODE_TAG, type.value());
        super.put(SystemConstant.MSG_TAG, msg);
        if (Objects.nonNull(data)) {
            super.put(SystemConstant.DATA_TAG, data);
        }
    }
    public JsonResult(ResultType type, String msg, long total, Object data){
        super.put(SystemConstant.CODE_TAG, type.value());
        super.put(SystemConstant.MSG_TAG, msg);
        super.put(SystemConstant.TOTAL_TAG, total);
        if (Objects.nonNull(data)){
            super.put(SystemConstant.DATA_TAG, data);
        }
    }

}