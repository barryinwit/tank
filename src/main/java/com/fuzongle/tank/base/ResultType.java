package com.fuzongle.tank.base;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/2 18:04
 * @Description:
 */
public enum ResultType {
    /**
     * 成功
     */
    SUCCESS(200),
    /**
     * 失败
     */
    ERROR(500);

    private final int value;

    ResultType(int value) {
        this.value = value;
    }

    public int value() {
        return this.value;
    }
}