package com.fuzongle.tank.base;

import org.apache.commons.lang3.StringUtils;

/**
 * @Auther: fuzongle
 * @Date: 2020/6/16 23:10
 * @Description:
 */
public class BaseService {

    /**
     *排序方法
     */
    public  String getOrderBy(String orderByColumn, String sort) {
        if (StringUtils.isEmpty(orderByColumn)) {
            return "";
        }
        if (StringUtils.isEmpty(sort)) sort = "desc";
        return orderByColumn + " " + sort;
    }

}