/*
Navicat MySQL Data Transfer

Source Server         : 121.43.184.444
Source Server Version : 50730
Source Host           : 121.43.184.44:3306
Source Database       : tank

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2020-06-21 21:24:49
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for demand
-- ----------------------------
DROP TABLE IF EXISTS `demand`;
CREATE TABLE `demand` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `title` varchar(255) DEFAULT NULL COMMENT '需求标题',
  `demand_type_id` int(255) DEFAULT NULL COMMENT '需求类型id',
  `demand_describe` varchar(9999) DEFAULT NULL COMMENT '需求描述',
  `technology` varchar(255) DEFAULT NULL COMMENT '技术要求',
  `contact_name` varchar(255) DEFAULT NULL COMMENT '联系人姓名',
  `contact_number` varchar(255) DEFAULT NULL COMMENT '联系人电话 ',
  `estimated_duration` int(255) DEFAULT NULL COMMENT '预计时长',
  `money_reward` double(255,0) DEFAULT NULL COMMENT '赏金',
  `publisher_id` int(11) DEFAULT NULL COMMENT '发布人id',
  `publisher_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '发布时间',
  `snatcher_id` int(11) DEFAULT NULL COMMENT '抢单人id',
  `snatcher_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '抢单时间',
  `status` int(255) DEFAULT '2' COMMENT '1.待发布  2.待接单 3进行中 4已完成',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='商机表';

-- ----------------------------
-- Records of demand
-- ----------------------------
INSERT INTO `demand` VALUES ('6', '急需一个商城网站!', null, '有偿', 'PHP', '付张三', '1231312', '10', '1000', '1', '2020-06-17 17:30:02', null, null, '2');

-- ----------------------------
-- Table structure for feedback
-- ----------------------------
DROP TABLE IF EXISTS `feedback`;
CREATE TABLE `feedback` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '反馈id',
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `content` varchar(9999) DEFAULT NULL COMMENT '内容',
  `status` int(11) DEFAULT '1' COMMENT '状态：1未解决 2已解决',
  `user_id` int(11) DEFAULT NULL COMMENT '反馈人id',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  `contact_number` varchar(64) DEFAULT NULL COMMENT '联系电话',
  `contact_name` varchar(64) DEFAULT NULL COMMENT '联系名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='反馈表';

-- ----------------------------
-- Records of feedback
-- ----------------------------
INSERT INTO `feedback` VALUES ('1', '网站不错', '建议升级', '1', '1', '2020-06-21 13:24:09', '联系人', '手机号');

-- ----------------------------
-- Table structure for leaving_message
-- ----------------------------
DROP TABLE IF EXISTS `leaving_message`;
CREATE TABLE `leaving_message` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `content` varchar(9999) DEFAULT NULL COMMENT '内容',
  `contact_number` varchar(11) DEFAULT NULL COMMENT '联系人电话',
  `contact_name` int(11) DEFAULT NULL COMMENT '联系名字',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='留言表';

-- ----------------------------
-- Records of leaving_message
-- ----------------------------

-- ----------------------------
-- Table structure for login_log
-- ----------------------------
DROP TABLE IF EXISTS `login_log`;
CREATE TABLE `login_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '登录日志记录表',
  `login_account` varchar(255) DEFAULT NULL COMMENT '登录帐号',
  `ip` varchar(255) DEFAULT NULL COMMENT '登录ip',
  `browser` varchar(255) DEFAULT NULL COMMENT '浏览器标识',
  `login_place` varchar(255) DEFAULT NULL COMMENT '登录地点',
  `operating_system` varchar(255) DEFAULT NULL COMMENT '操作系统',
  `status` tinyint(1) DEFAULT NULL COMMENT '1 正常 2失败',
  `info` varchar(255) DEFAULT NULL COMMENT '登录·详细信息',
  `login_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '登录时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=672 DEFAULT CHARSET=utf8 COMMENT='登录日志表';

-- ----------------------------
-- Records of login_log
-- ----------------------------
INSERT INTO `login_log` VALUES ('655', 'tank', '127.0.0.1', 'Chrome 8', '内网', 'Windows 10', '1', '登录成功', '2020-06-16 23:52:00');

-- ----------------------------
-- Table structure for mail_record
-- ----------------------------
DROP TABLE IF EXISTS `mail_record`;
CREATE TABLE `mail_record` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '标题',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `receiver_mail` varchar(255) DEFAULT NULL COMMENT '接收人邮箱',
  `send_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '发送时间',
  `status` varchar(255) DEFAULT NULL COMMENT '1成功 2失败',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='邮件发送记录表';

-- ----------------------------
-- Records of mail_record
-- ----------------------------
INSERT INTO `mail_record` VALUES ('6', '测试', '测试内容', '583059767@qq.com', '2020-06-17 00:32:38', '1');

-- ----------------------------
-- Table structure for open_log
-- ----------------------------
DROP TABLE IF EXISTS `open_log`;
CREATE TABLE `open_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL COMMENT '操作标题',
  `service_type` varchar(255) DEFAULT NULL COMMENT '0其他 1添加 2修改 3删除',
  `package_name` varchar(255) DEFAULT NULL COMMENT '包名',
  `method_name` varchar(255) DEFAULT NULL COMMENT '方法名字',
  `request_mode` varchar(255) DEFAULT NULL COMMENT '请求方式',
  `request_url` varchar(255) DEFAULT NULL COMMENT '请求地址',
  `request_ip` varchar(255) DEFAULT NULL COMMENT '请求ip',
  `request_place` varchar(255) DEFAULT NULL COMMENT '请求地点',
  `request_browser` varchar(255) DEFAULT NULL COMMENT '浏览器',
  `status` varchar(255) DEFAULT NULL COMMENT '状态：1正常 2异常',
  `open_name` varchar(255) DEFAULT NULL COMMENT '操作人',
  `open_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '操作时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of open_log
-- ----------------------------
INSERT INTO `open_log` VALUES ('24', '删除日志', '2', 'com.fuzongle.tank.controller.LoginLogController', 'delete', 'GET', '/admin/loginLog/delete', '127.0.0.1', '内网', 'Chrome 8', '1', 'xxx', '2020-06-16 23:51:41');

-- ----------------------------
-- Table structure for picture
-- ----------------------------
DROP TABLE IF EXISTS `picture`;
CREATE TABLE `picture` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `url` varchar(255) DEFAULT NULL COMMENT '图片地址',
  `name` varchar(255) DEFAULT NULL COMMENT '图片名字',
  `download_num` int(255) DEFAULT '0' COMMENT '下载次数',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='图片上传下载表';

-- ----------------------------
-- Records of picture
-- ----------------------------
INSERT INTO `picture` VALUES ('14', 'static/picture/PcStore/tank2084264430.067452298135.jpg', '1231223', '0', '2020-06-17 01:17:29');

-- ----------------------------
-- Table structure for task_conf
-- ----------------------------
DROP TABLE IF EXISTS `task_conf`;
CREATE TABLE `task_conf` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '任务名字',
  `class_name` varchar(255) DEFAULT NULL,
  `cron` varchar(255) DEFAULT NULL COMMENT 'cron表达式',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL,
  `status` int(255) DEFAULT '2' COMMENT '1启动 2关闭',
  `remarks` varchar(255) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='任务配置表';

-- ----------------------------
-- Records of task_conf
-- ----------------------------
INSERT INTO `task_conf` VALUES ('2', '测试111', 'com.fuzongle.tank.task.TestTask', '*/8 * * * * ?', '2020-06-11 15:00:10', '2020-06-11 15:00:10', '2', '备注1111');

-- ----------------------------
-- Table structure for test
-- ----------------------------
DROP TABLE IF EXISTS `test`;
CREATE TABLE `test` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '名字',
  `age` int(11) DEFAULT NULL COMMENT '年龄',
  `create_time` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='测试数据表';

-- ----------------------------
-- Records of test
-- ----------------------------
INSERT INTO `test` VALUES ('2', '测试数据', '1', '2020-05-17 19:15:32.312000');
INSERT INTO `test` VALUES ('3', '测试数据', '2', '2020-05-17 19:16:00.312000');
INSERT INTO `test` VALUES ('4', '测试数据', '2', '2020-05-17 19:20:38.381000');
INSERT INTO `test` VALUES ('7', '测试数据', '3', '2020-05-17 21:19:11.398000');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户表',
  `name` varchar(64) DEFAULT NULL COMMENT '用户名字',
  `account` varchar(64) DEFAULT NULL COMMENT '用户登录帐号',
  `password` varchar(255) DEFAULT NULL COMMENT '用户密码',
  `sex` tinyint(1) DEFAULT NULL COMMENT '1男 2女',
  `create_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='系统用户表';

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', 'tank', 'e10adc3949ba59abbe56e057f20f883e', '1', '2020-06-04 21:44:26');
